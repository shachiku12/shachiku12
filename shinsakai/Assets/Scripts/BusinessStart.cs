﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusinessStart : MonoBehaviour
{
    //public GameObject Data;
    public DataManager dataManager;
    // Start is called before the first frame update
    void Start()
    {
        dataManager = DataManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void MoneyProccesing()
    {
        float m = Random.Range(1f, 5f);
        int mPush = (int)(m * 1000000);
        dataManager.Data.saveMoneyStatus += mPush;
        Debug.Log(dataManager.Data.saveMoneyStatus);
    }
}
