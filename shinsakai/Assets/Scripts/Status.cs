﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
[CreateAssetMenu(fileName = "StatusData", menuName = "StatusData")]

public class Status : ScriptableObject
{
    public int[] charaStatus01 = new int[5];
    public int[] charaStatus02 = new int[5];
}
