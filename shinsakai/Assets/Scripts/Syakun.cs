﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Syakun : MonoBehaviour
{
    public GameObject ManagerObj;
    Manager managerScript;
    public tableObjectScript table;
    //DataManager.SaveData data;
    //public DataManager dataManager;
    // Start is called before the first frame update
    void Start()
    {
        managerScript = Manager.Instance;
        //data = dataManager.Data;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    int[] c01 = new int[5];
    public void GoSyakunScene()
    {
        
        managerScript.SyakunProcessing(c01);
        int cnt = 0;
        table.outputDataList[8].messageText = "";
        while (true)
        {
            if (cnt > 4)
            {
                Debug.Log(table.outputDataList[8].messageText);
                break;
            }
            
            string unko = managerScript.stateName[cnt];
            string s = unko + "が" + c01[cnt] + "あがった！\n";
            
            table.outputDataList[8].messageText += s;
            
            cnt++;
            
        }
        
        SceneManager.LoadScene("社訓唱和");
    }
}
