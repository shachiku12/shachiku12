﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    DataManager dataManager;
    public GameObject DataManagerObj;

    public GameObject SaveSlotText01;
    public GameObject SaveSlotText02;
    public GameObject SaveSlotText03;
    public GameObject SaveSlotText04;
    public GameObject SaveSlotText05;
    public GameObject SaveSlotText06;

    public GameObject SaveSlotNum01;
    public GameObject SaveSlotNum02;
    public GameObject SaveSlotNum03;

    DataManager.SaveData data;
    // Start is called before the first frame update
    void Start()
    {
        dataManager = DataManager.Instance;
        data = dataManager.Data;

        //dataManager.TmpLoad();

        dataManager.TmpSave();
        TextDasuyo();
        dataManager.TmpLoad();
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void TextOutput(string ss)
    {
        dataManager.Save(ss);
        TextDasuyo();
    }

    public void DataLoad(string ss)
    {
        dataManager.Load(ss);
    }

    void TextOutput1(DataManager.SaveData data, GameObject textObj)
    {
        Text text = textObj.GetComponent<Text>();
        text.text =
            "作業効率:" + data.saveStatus01[0] +
            "信仰心:" + data.saveStatus01[1] +
            "体力:" + data.saveStatus01[2] +
            "\n精神力:" + data.saveStatus01[3] +
            "レベル:" + data.saveStatus01[4];
    }
    void TextOutput2(DataManager.SaveData data, GameObject textObj)
    {
        Text text = textObj.GetComponent<Text>();
        text.text =
            "作業効率:" + data.saveStatus02[0] +
            "信仰心:" + data.saveStatus02[1] +
            "体力:" + data.saveStatus02[2] +
            "\n精神力:" + data.saveStatus02[3] +
            "レベル:" + data.saveStatus02[4];
    }


    //"charakey" + 0 or 1 or 2;
    //作業効率=00信仰心=00体力=00
    //精神力=00レベル=00
    public void TextDasuyo()
    {



        DataManager.SaveData data = dataManager.GetData("slot1");
        TextOutput1(data, SaveSlotText01);
        TextOutput2(data, SaveSlotText02);
        SaveSlotNum01.GetComponent<Text>().text = "SAVESLOT" + "1";

        data = dataManager.GetData("slot2");
        TextOutput1(data, SaveSlotText03);
        TextOutput2(data, SaveSlotText04);
        SaveSlotNum02.GetComponent<Text>().text = "SAVESLOT" + "2";

        data = dataManager.GetData("slot3");
        TextOutput1(data, SaveSlotText05);
        TextOutput2(data, SaveSlotText06);
        SaveSlotNum03.GetComponent<Text>().text = "SAVESLOT" + "3";



        //dataManager.Load();
        //Text text;
        //int key = dataManager.keyNum;
        //switch (key)
        //{
        //    case 0:
        //        text = SaveSlotText01.GetComponent<Text>();
        //        text.text =
        //    "作業効率:" + dataManager.saveData.saveStatus01[0] +
        //    "信仰心:" + dataManager.saveData.saveStatus01[1] +
        //    "体力:" + dataManager.saveData.saveStatus01[2] +
        //    "\n精神力:" + dataManager.saveData.saveStatus01[3] +
        //    "レベル:" + dataManager.saveData.saveStatus01[4];

        //        text = SaveSlotText02.GetComponent<Text>();
        //        text.text =
        //            "作業効率:" + dataManager.saveData.saveStatus02[0] +
        //    "信仰心:" + dataManager.saveData.saveStatus02[1] +
        //    "体力:" + dataManager.saveData.saveStatus02[2] +
        //    "\n精神力:" + dataManager.saveData.saveStatus02[3] +
        //    "レベル:" + dataManager.saveData.saveStatus02[4];

        //        text = SaveSlotNum01.GetComponent<Text>();
        //        text.text = "SAVESLOT" + key.ToString();
        //        break;
        //    case 1:
        //        text = SaveSlotText03.GetComponent<Text>();
        //        text.text =
        //    "作業効率:" + dataManager.saveData.saveStatus01[0] +
        //    "信仰心:" + dataManager.saveData.saveStatus01[1] +
        //    "体力:" + dataManager.saveData.saveStatus01[2] +
        //    "\n精神力:" + dataManager.saveData.saveStatus01[3] +
        //    "レベル:" + dataManager.saveData.saveStatus01[4];

        //        text = SaveSlotText04.GetComponent<Text>();
        //        text.text =
        //            "作業効率:" + dataManager.saveData.saveStatus02[0] +
        //    "信仰心:" + dataManager.saveData.saveStatus02[1] +
        //    "体力:" + dataManager.saveData.saveStatus02[2] +
        //    "\n精神力:" + dataManager.saveData.saveStatus02[3] +
        //    "レベル:" + dataManager.saveData.saveStatus02[4];

        //        text = SaveSlotNum02.GetComponent<Text>();
        //        text.text = "SAVESLOT" + key.ToString();
        //        break;
        //    case 3:
        //        text = SaveSlotText05.GetComponent<Text>();
        //        text.text =
        //    "作業効率:" + dataManager.saveData.saveStatus01[0] +
        //    "信仰心:" + dataManager.saveData.saveStatus01[1] +
        //    "体力:" + dataManager.saveData.saveStatus01[2] +
        //    "\n精神力:" + dataManager.saveData.saveStatus01[3] +
        //    "レベル:" + dataManager.saveData.saveStatus01[4];

        //        text = SaveSlotText06.GetComponent<Text>();
        //        text.text =
        //            "作業効率:" + dataManager.saveData.saveStatus02[0] +
        //    "信仰心:" + dataManager.saveData.saveStatus02[1] +
        //    "体力:" + dataManager.saveData.saveStatus02[2] +
        //    "\n精神力:" + dataManager.saveData.saveStatus02[3] +
        //    "レベル:" + dataManager.saveData.saveStatus02[4];

        //        text = SaveSlotNum03.GetComponent<Text>();
        //        text.text = "SAVESLOT" + key.ToString();
        //        break;

        //}
    }
}
