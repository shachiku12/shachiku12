﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
[CreateAssetMenu(fileName = "DeadTextData", menuName = "DeadTextData")]

public class DeadDataBase : ScriptableObject
{
    public enum PositionType
    {
        Left,
        Center,
        Right,
        Num,
    }
    [Serializable]
    public class DeadMessageData
    {
        [SerializeField] public PositionType positionType = PositionType.Left;
        [SerializeField] public string nameText = "";
        [SerializeField, Multiline(4)] public string messageText = "";
        [SerializeField] public Sprite characterImage = null;
    }
    [SerializeField] public DeadMessageData[] deadTexts;
}
