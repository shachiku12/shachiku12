﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SVScript : MonoBehaviour
{
    Animator anim;
    public Sprite chara01sprite;
    public Sprite chara02sprite;
    public GameObject CharaImageObj;
    Manager managerScript;
    public GameObject managerObj;
    Image cImage;
    Text charaStateText;
    public GameObject CharacterStateTextObj;
    int cnt = 0;
    //public Status statusScript;
    public DataManager dataManager;

    DataManager.SaveData data;

    // Start is called before the first frame update
    void Start()
    {
        dataManager = DataManager.Instance;
        anim = GetComponent<Animator>();
        cImage = CharaImageObj.GetComponent<Image>();
        managerScript = Manager.Instance;
        charaStateText = CharacterStateTextObj.GetComponent<Text>();
        data = dataManager.Data;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //0作業効率、1信仰心、2体力、3精神力、4レベル
    public void FromCharacter01()
    {

        cImage.sprite = chara01sprite;

        managerScript.chara01 = true;
        managerScript.chara02 = false;
        charaStateText.text =
            "氏名:キャサリン \n性別:女 年齢:26\nステータス:" +
            "\n作業効率:" + data.saveStatus01[0] +
            "\n信仰心:" + data.saveStatus01[1] +
            "\n体力:" + data.saveStatus01[2] +
            "\n精神力:" + data.saveStatus01[3] +
            "\nレベル:" + data.saveStatus01[4];
        ScrollViewAnimation();
    }

    public void FromCharacter02()
    {
        cImage.sprite = chara02sprite;

        managerScript.chara02 = true;
        managerScript.chara01 = false;
        charaStateText.text =
            "氏名:働気　太蔵 \n性別:男 年齢:22\nステータス:" +
            "\n作業効率:" + data.saveStatus02[0] +
            "\n信仰心:" + data.saveStatus02[1] +
            "\n体力:" + data.saveStatus02[2] +
            "\n精神力:" + data.saveStatus02[3] +
            "\nレベル:" + data.saveStatus02[4];
        ScrollViewAnimation();
    }

    void ScrollViewAnimation()
    {

        cnt++;

        if (cnt % 2 == 0)
        {
            anim.SetBool("Close", true);
            anim.SetBool("Open", false);

        }
        else if (cnt % 2 == 1)
        {
            anim.SetBool("Close", false);
            anim.SetBool("Open", true);

        }

    }
}
