﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "AdventureData", menuName = "CreateAdventureData")]

public class tableObjectScript : ScriptableObject
{
    public enum PositionType
    {
        Left,
        Center,
        Right,
        Num,
    }

    //配列にするデータ構造を定義する
    [Serializable]
    public class MessageData
    {
        [SerializeField] public PositionType positionType = PositionType.Left;
        [SerializeField] public string nameText = "";
        [SerializeField, Multiline(4)] public string messageText = "";
        [SerializeField] public Sprite characterImage = null;
    }
    

    //データの配列
    [SerializeField] public MessageData[] outputDataList;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }
    //0作業効率、1信仰心、2体力、3精神力、4レベル
    // Update is called once per frame
    void Update()
    {
        
    }
}
