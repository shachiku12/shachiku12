﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TextScript : MonoBehaviour
{
    public tableObjectScript table;
    Text text;
    int cnt = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextText()
    {
        if (cnt > 8)
        {
            //訓練が終わり、そのキャラは一日訓練できない。
            SceneManager.LoadScene("訓練menu");
        }
        else
        {
            string s = table.outputDataList[cnt].messageText;
            text.text = s;

            cnt++;
        }
    }
}
