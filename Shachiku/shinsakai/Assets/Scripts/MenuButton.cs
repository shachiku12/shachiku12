﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClickFromHome()
    {
        SceneManager.LoadScene("Home");
    }
    public void OnClickFromDrill()
    {
        SceneManager.LoadScene("訓練menu");
    }
    public void OnClickFromBusiness()
    {
        SceneManager.LoadScene("業務");
    }
}
