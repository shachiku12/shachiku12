﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioHome : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip audio;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = audio;
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
