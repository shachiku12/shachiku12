﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Roulette : MonoBehaviour
{
    public GameObject startButton;
    public GameObject stopButton;

    private float nowSpeed;
    private float MAX_SPEED = 55f;
    private bool isStart = false;
    private bool isStop = false;
    private float oneGrid = 28 / 360;
    // Start is called before the first frame update
    void Start()
    {
        stopButton.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isStart)
        {
            if (!isStop)
            {
                if (nowSpeed < MAX_SPEED)
                {
                    nowSpeed += 5f;
                }
                else
                {
                    stopButton.SetActive(true);
                }
            }
            else
            {
                nowSpeed -= 5f;
            }

            transform.localEulerAngles = new Vector3(0, 0, transform.localEulerAngles.z + nowSpeed * -Time.deltaTime);

            if (nowSpeed <= 0)
            {
                isStart = false;
                isStop = false;
                startButton.SetActive(true);
                int num = Mathf.FloorToInt(transform.localEulerAngles.z / oneGrid);


            }
        }
    }
}
