﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeadText : MonoBehaviour
{
    public GameObject GameOverTextObj;
    public DeadDataBase DDBScript;
    Text Text;
    //Manager manager;
    //public GameObject managerObj;
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {

        anim = GameOverTextObj.GetComponent<Animator>();
        Text = GetComponent<Text>();
        //manager = Manager.Instance;
    }

    int cnt = 0;
    public void DeadTextKansuu()
    {
        if (cnt == 7)
        {
            anim.SetBool("GoGameOver", true);
            
        }
        else if (cnt == 8)
        {
            SceneManager.LoadScene("Home");
        }
        else if (cnt == 1)
        {
            string s = "キャサリン";
            DDBScript.deadTexts[1].messageText = s + "が過労死しました。";
            Text.text = DDBScript.deadTexts[1].messageText;
        }
        else
        {
            Text.text = DDBScript.deadTexts[cnt].messageText;
        }

        cnt++;
    }
}
