﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DataManager : MonoSingleton<DataManager>
{
    #region enum
    public enum SaveSlot
    {
        SlotA,
        SlotB,
        SlotC,
    }
    #endregion

    public const int SaveCharaStatusSlot = 3;
    public int keyNum = 0;
    public string key = "charakey";
    string tmpkey = "tmp";


    [System.Serializable]
    public class SaveData
    {
        public int[] saveStatus01 = new int[5];
        public int[] saveStatus02 = new int[5];
        public int saveMoneyStatus = 0;

        public SaveData(int[] char1, int[] char2, int money)
        {
            saveStatus01 = char1;
            saveStatus01 = char1;
            saveMoneyStatus = money;
        }

    }
    private void Start()
    {
        //TmpLoad();

        //Debug.Log(saveData.saveStatus01[0] + "" + saveData.saveStatus01[1]);

        SceneManager.sceneLoaded += ((scene, mode) => { TmpLoad(); });
        SceneManager.sceneUnloaded += ((scene) => { TmpSave(); });

    }



    public SaveData Data
    {
        get
        {
            /*TmpLoad();*/
            return saveData;
        }
        set
        {
            saveData = value;/* TmpSave();*/
        }
    }

    SaveData defData = new SaveData(new int[5] { 0, 0, 0, 0, 0 }, new int[5] { 0, 0, 0, 0, 0 }, 0);
    SaveData saveData;
    /// <summary>ボタン用</summary>
    public void Save()
    {
        string s = JsonUtility.ToJson(saveData);
        PlayerPrefs.SetString(key, s);
        PlayerPrefs.Save();
    }
    /// <summary>ボタン用</summary>
    public void Load()
    {
        string s = PlayerPrefs.GetString(key);
        saveData = JsonUtility.FromJson<SaveData>(s);
        if (saveData == null) { saveData = defData; }
    }
    public void Save(string s)
    {
        string json = JsonUtility.ToJson(saveData);
        PlayerPrefs.SetString(s, json);
        PlayerPrefs.Save();
    }
    /// <summary>ボタン用</summary>
    public void Load(string s)
    {
        string json = PlayerPrefs.GetString(s);
        saveData = JsonUtility.FromJson<SaveData>(json);
        if (saveData == null) { saveData = defData; }
        //TmpSave();
    }

    public void TmpSave()
    {
        string s = JsonUtility.ToJson(saveData);
        PlayerPrefs.SetString(tmpkey, s);
        PlayerPrefs.Save();
    }

    public void TmpLoad()
    {
        string s = PlayerPrefs.GetString(tmpkey);
        saveData = JsonUtility.FromJson<SaveData>(s);
        if (saveData == null) { saveData = defData; }
    }


    public SaveData GetData(string key)
    {
        string s = PlayerPrefs.GetString(key);
        SaveData tmp = JsonUtility.FromJson<SaveData>(s);
        if (tmp == null) { tmp = defData; }
        return tmp;
    }


    //Saveしてある0作業効率、1信仰心、2体力、3精神力、4レベル
    //日時Datetime.Now Year、Month、Day、Hour、Minute、Second、Millisecond
    //をsaveTextに表示
    //
    private void KeyChange()
    {
        key = "charakey";
        key += keyNum;
    }
    public void GetKeyNum01()
    {
        keyNum = 0;
        KeyChange();
    }
    public void GetKeyNum02()
    {
        keyNum = 1;
        KeyChange();
    }
    public void GetKeyNum03()
    {
        keyNum = 2;
        KeyChange();
    }
    public override void OnInitialize()
    {
        Debug.Log("a");
        base.OnInitialize();
        DontDestroyOnLoad(Instance);
    }
}
