﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;
using UnityEngine.SceneManagement;

public class Manager : MonoSingleton<Manager>
{
    Text TotalMoneyText;
    public GameObject MoneyObj;
    public GameObject WarningObj;
    Animator warningAnim;
    public GameObject ScrollViewObj;
    public bool chara01 = false;
    public bool chara02 = false;
    public string charaState = "キャサリン";
    public DataManager dataM;
    //public Status statusScript;
    public string[] stateName = new string[5] { "作業効率", "信仰心", "体力", "精神力", "レベル" };

    DataManager.SaveData data;
    // Start is called before the first frame update
    void Start()
    {
        dataM = DataManager.Instance;
        //warning = WarningObj.GetComponent<Image>();
        data = dataM.Data;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(chara01);
        //Debug.Log(chara02);
    }
    public int[] SyakunProcessing(int[] c01)//社訓
    {
        //信仰心、精神力、レベル　<-- アップ
        //体力、　<-　ダウン
        //0作業効率、1信仰心、2体力、3精神力、4レベル
        if (chara01)
        {
            float a = 0;
            int b = 0;
            a = Random.Range(-1.6f, 8.0f);//信仰心
            data.saveStatus01[1] += (int)a;//信仰心
            c01[1] = (int)a;
            b += (int)a;
            //Debug.Log(statusScript.charaStatus01[1]);

            a = Random.Range(-2.4f, 10.0f);//精神力
            data.saveStatus01[3] += (int)a;//精神力
            c01[3] = (int)a;
            b += (int)a;

            a = Random.Range(-1.0f, -4.4f);//体力
            data.saveStatus01[2] += (int)a;//体力
            c01[2] = (int)a;
            b += (int)a;

            a = Random.Range(1.0f, 6.5f);//レベル
            data.saveStatus01[4] += (int)a;//レベル
            c01[4] = b;

            charaState = "キャサリン";

            CheckCharacterState();
        }
        else if (chara02)
        {
            float a = 0;
            int b = 0;
            a = Random.Range(-1.6f, 8.0f);//信仰心
            data.saveStatus02[1] += (int)a;//信仰心
            c01[1] = (int)a;
            b += (int)a;
            //Debug.Log(statusScript.charaStatus01[1]);

            a = Random.Range(-2.4f, 10.0f);//精神力
            data.saveStatus02[3] += (int)a;//精神力
            c01[3] = (int)a;
            b += (int)a;

            a = Random.Range(-1.0f, -4.4f);//体力
            data.saveStatus02[2] += (int)a;//体力
            c01[2] = (int)a;
            b += (int)a;

            a = Random.Range(1.0f, 6.5f);//レベル
            data.saveStatus02[4] += (int)a;//レベル
            //レベルをこのゲームにおいて、どういう扱いにするのか
            c01[4] = b;

            charaState = "働気　太蔵";

            CheckCharacterState();
        }


        dataM.Data = data;

        //for (int cnt = 0; cnt < 5; cnt++)
        //{
        //    dataM.saveData.saveStatus01[cnt] = statusScript.charaStatus01[cnt];
        //    dataM.saveData.saveStatus02[cnt] = statusScript.charaStatus02[cnt];
        //}


        return c01;
    }
    void CheckCharacterState()
    {
        warningAnim = WarningObj.GetComponent<Animator>();
        if (data.saveStatus01[2] < -20)
        {
            float time = 0;
            warningAnim.SetBool("Life", false);
            warningAnim.SetBool("Dead", true);
            while (time < 1.1f)
            {
                time += Time.deltaTime;
            }
            chara01 = true;
            chara02 = false;
            SceneManager.LoadScene("DeadCharacter");
        }
        else if (data.saveStatus02[2] < -20)
        {
            float time = 0;
            warningAnim.SetBool("Life", false);
            warningAnim.SetBool("Dead", true);
            while (time < 1.1f)
            {
                time += Time.deltaTime;
            }
            chara01 = false;
            chara02 = true;
            SceneManager.LoadScene("DeadCharacter");
        }
        else
        {
            warningAnim.SetBool("Dead", false);
            warningAnim.SetBool("Life", true);
        }
    }
}
